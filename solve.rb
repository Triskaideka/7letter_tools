#!/usr/bin/ruby

=begin
solve.rb

Given a file that contains a grid of letters, tries to find all of the words
that can be formed from that grid, according to the puzzle's rules.
=end



### Libraries
require 'optparse'



### Input validation
options = {:cards => false, :points => false, :save => false, :save_only => false}

option_parser = OptionParser.new { |opts|

  opts.banner = "usage: #{$0} [-achpsS] <puzzle_to_solve.txt>"

  opts.on('-a', '--analyze', "Also print statistics about the puzzle") {
    options[:analyze] = true;
  }

  opts.on('-c', '--cards', "Also show the cards in the hand that forms the word") {
    options[:cards] = true;
  }

  opts.on('-h', '--help', "Show this help") {
    puts opts
    exit
  }

  opts.on('-p', '--points', "Also print how many points each word is worth") {
    options[:points] = true;
  }

  opts.on('-s', '--save', "Save the solutions to a file") {
    options[:save] = true;
  }

  opts.on('-S', '--save-only', "Save solutions to a file and don't print them on the screen") {
    options[:save_only] = true;
  }

}

option_parser.parse!
#puts options[:points]



### Variables
# input files
dict_filename = "dictionaries/7dict.txt"

if ARGV.length < 1 then
  puts option_parser
  exit
end
puzzle_filename = ARGV[0]

# Output file.
output_filename = puzzle_filename.gsub(/^.*\//, '')  # strip everything up to the last slash
output_filename = "./solutions/" + output_filename  # put all solution files in a "solutions" subdirectory

# The lengths of short and long sets of cards.
short = 3
long = 4

# For storing all possible book permutations.
short_books = []
long_books = []

# Ace through King is 13 cards.
# We could do this puzzle with any number of cards, but should set a sensible
# limit.
max_cards_per_suit = 13

# The puzzle grid is a two-dimensional array that holds the cards in the puzzle.
grid = []

# An array containing all of the solutions that this program finds.
solutions = []

# For separating parts of the output.
divider = "--------------------"



### The Card class.
class Card

  attr_reader :identity, :letter, :suit, :rank, :points

  def initialize(s, r, l)

    raise "Suit argument is wrong type" unless s.is_a?(Integer)
    raise "Rank argument is wrong type" unless r.is_a?(Integer)
    raise "Letter argument is wrong type" unless l.is_a?(String)

    @letter = l
    @suit = s

    @rank = r
    @points = r + 1  # internally, ranks start at 0 (the Ace) but points start at 1
    if @points > 10 then
      @points = 10
    end

    @suits = ["Diamonds", "Clubs", "Hearts", "Spades"]
    @suit_symbols = ["♦", "♣", "♥", "♠"]
    @ranks = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
    @identity = @ranks[r] + @suit_symbols[s]

  end

  def to_i
    @points
  end

  def to_s
    @letter
  end

end   # class Card



### Load the dictionary from its file into an array
dictionary = IO.readlines(dict_filename, chomp: true)

#puts dictionary.include?("WESTERN")
#puts dictionary.include?("UNCLEAR")
#puts dictionary



### Load the puzzle from its file into a two-dimensional array of card objects
puzzle = IO.readlines(puzzle_filename, chomp: true)

puzzle.each_index { |suit|

  # truncate the line to the maximum number of cards
  puzzle[suit] = puzzle[suit][0...max_cards_per_suit]

  # create the second-level array to hold the cards in each suit
  grid[suit] = []

  # create each card object
  puzzle[suit].split(//).each_index { |rank|
    grid[suit][rank] = Card.new(suit, rank, puzzle[suit][rank])
  }

}

#puts grid.inspect
#grid.each{ |s| puts s.map{ |c| c.to_s }.join }
#exit



### Get all possible book permutations
(0...grid[0].length).each { |rank|

  book = [ grid[0][rank], grid[1][rank], grid[2][rank], grid[3][rank] ]

  short_books += book.permutation(short).to_a
  long_books += book.permutation(long).to_a

}

#puts short_books.inspect
#puts short_books.collect{ |b| b.map{ |c| c.to_s }.join }.join(", ")
#exit



### Iterate through the grid.
### This loop has two major subsections that are copy-and-pastes of each other, with "short" and "long" switched.
grid.each { |suit|

  # iterate through each of the letters that could start a SHORT run
  ( 0..(suit.length - short) ).each { |i|

    long_books.each { |perm|

      run = suit[i...i+short]
      book = perm

      # try combining the book and run in either order and see if it makes a word
      [run + book, book + run].each { |hand|

        # Don't consider this as a possible word if it contains any duplicate cards.
        if ( hand != hand.uniq ) then
          next
        end

        # assemble the letters
        possible_word = hand.map{ |c| c.to_s }.join

        if dictionary.include?(possible_word) then

          solution = Hash.new

          solution[:word] = possible_word  # it's no longer just "possible"; it IS a word
          solution[:points] = hand.reduce(0){ |sum, card| sum + card.points }.to_s
          solution[:cards] = hand.collect{ |card| card.identity }.join('-')



          # Check if this word has already been found (which would mean it can be made in more than one way).
          earlier_solution = solutions.find{ |competing_solution| solution[:word] == competing_solution[:word] }

          # If there was no earlier solution, just add the new one.
          if ( earlier_solution.nil? ) then

            solutions.push(solution)

          # If the new solution is worth more points, delete the earlier solution and add the new one.
          elsif ( solution[:points].to_i > earlier_solution[:points].to_i ) then

            solutions.reject!{ |solution| solution[:word] == earlier_solution[:word] }
            solutions.push(solution)

          end
          # If there was an earlier solution and IT was worth more or the same amount of points,
          # then we won't have deleted it or added the new one.



          # print the solution that we just found, unless the user asked us not to
          if ( ! options[:save_only] ) then

            print solution[:word]

            if (options[:points]) then
              print " " + solution[:points]
            end

            if (options[:cards]) then
              print " " + solution[:cards]
            end

            print "\n"

          end

        end

      }  # end the loop that tests run+book and then book+run

    }  # end looping over each of the long books

  }  # end .each for each of the letters that could start a short run



  # iterate through each of the letters that could start a LONG run
  ( 0..(suit.length - long) ).each { |i|

    short_books.each { |perm|

      run = suit[i...i+long]
      book = perm

      # try combining the book and run in either order and see if it makes a word
      [run + book, book + run].each { |hand|

        # Don't consider this as a possible word if it contains any duplicate cards.
        if ( hand != hand.uniq ) then
          next
        end

        # assemble the letters
        possible_word = hand.map{ |c| c.to_s }.join

        if dictionary.include?(possible_word) then

          solution = Hash.new

          solution[:word] = possible_word  # it's no longer just "possible"; it IS a word
          solution[:points] = hand.reduce(0){ |sum, card| sum + card.points }.to_s
          solution[:cards] = hand.collect{ |card| card.identity }.join('-')



          # Check if this word has already been found (which would mean it can be made in more than one way).
          earlier_solution = solutions.find{ |competing_solution| solution[:word] == competing_solution[:word] }

          # If there was no earlier solution, just add the new one.
          if ( earlier_solution.nil? ) then

            solutions.push(solution)

          # If the new solution is worth more points, delete the earlier solution and add the new one.
          elsif ( solution[:points].to_i > earlier_solution[:points].to_i ) then

            solutions.reject!{ |solution| solution[:word] == earlier_solution[:word] }
            solutions.push(solution)

          end
          # If there was an earlier solution and IT was worth more or the same amount of points,
          # then we won't have deleted it or added the new one.



          # print the solution that we just found, unless the user asked us not to
          if ( ! options[:save_only] ) then

            print solution[:word]

            if (options[:points]) then
              print " " + solution[:points]
            end

            if (options[:cards]) then
              print " " + solution[:cards]
            end

            print "\n"

          end

        end

      }  # end the loop that tests run+book and then book+run

    }  # end looping over each of the short books

  }  # end .each for each of the letters that could start a long run

}



# If there aren't any solutions, we can quit now.
if ( solutions.length <= 0 ) then
  puts divider
  puts "No solutions in this puzzle!"
  exit
end



### Print the analysis, if requested.
if (options[:analyze]) then

  card_usage = Hash.new(0)

  # Count how many times each card appears in a solution.
  solutions.each { |sol|
    sol[:cards].split(/-/).each { |card|
      card_usage[card] += 1
    }
  }

  card_utilization = grid.length * grid[0].length

  # Figure out which card is used the most.
  # (In the case of a tie, we only report one.)
  most_used_card = card_usage.keys[0]

  card_usage.each { |card, times|
    if times > card_usage[most_used_card] then
      most_used_card = card
    end
  }

  # Print output.
  puts divider

  puts "Number of solutions: #{solutions.length}"

  puts "Card utilization: #{card_usage.length}/#{card_utilization} " +
       "(#{(card_usage.length.to_f / card_utilization * 100).round(1)}%)"

  puts "Most used card: #{most_used_card} (#{card_usage[most_used_card]} times)"

end



### Save the solutions in a file, if requested.
if ( (options[:save] || options[:save_only]) ) then

  output_file = File.open(output_filename, "w")

  solutions
    .map { |sol|
      # turn the hash into an array of text lines that are suitable for writing to the file
      sol[:word] +
        ( options[:points] ? " #{sol[:points]}" : "") +
        ( options[:cards] ? " #{sol[:cards]}" : "")
    }
    .sort  # now that they're strings, it's easier to sort them
    .each { |sol|
      output_file.write(sol + "\n")
    }

  output_file.close

  puts divider
  puts "Saved #{solutions.length} solutions in #{output_filename}"

end
