# 7-letter tools

This is a collection of programs, written in a whimisical mixture of Ruby and Perl,
that will *compose* and *solve* word puzzles of a certain type.

In this type of puzzle, the solver is presented with a grid of 52 "cards", a letter of the
alphabet printed on each one, and asked to identify the seven-letter English words that may be formed by
selecting certain three-card and four-card subsets of the grid.

Word puzzles like this are published often in
[GAMES WORLD OF PUZZLES magazine](https://gamesmagazine-online.com/) under the name "Word Rummy"
or "500 Rummy".  However, these tools (and their author) are not affiliated with the magazine
nor its publishers.

To create [the dictionary of seven-letter words](dictionaries/7dict.txt),
I downloaded the SOWPODS and ENABLE English language dictionaries from
[wordgamedictionary.com](https://www.wordgamedictionary.com/dictionary/), combined them,
extracted the seven-letter words, and removed duplicates.  (That's what
[dictionaries/make_dict.perl](dictionaries/make_dict.perl) is for.)

## Instructions

If you want to use these programs, here are some instructions to get you started.

Make sure you have [Perl](https://www.perl.org/) and [Ruby](https://www.ruby-lang.org/)
installed on your system.  Users of Unix-like operating systems will likely already have them; others
should visit the preceding links to download & install.

The composer program makes use of a text file called "common_sequences.txt".  This file is not part
of the repository, but it is created by one of the programs that is.  Do this first:

    cd dictionaries
    perl ./find_common_sequences.perl > common_sequences.txt
    cd ..

The solve script uses a brute-force approach and the included dictionaries to find every word in the
given puzzle.  It can solve a puzzle in about half a minute on my computer, but may run slower or
faster on yours.  Try these commands:

    ruby ./solve.rb -h
    ruby ./solve.rb -cs puzzles/example1.txt

The compose script is, at present, not very good.  It frequently produces puzzles with few or no
solutions, and when it does produce ones with many solutions, those solutions are often overly similar
to one another.

    ruby ./compose.rb -h
    ruby ./compose.rb

To cope with the composer's unreliability, there is a "mine" script.  The miner repeatedly composes puzzles,
solves them, and decides (based on configurable criteria) whether they are good enough to be kept.  For
those that are, the puzzle files can be found in the "puzzles" directory and the solutions will be in
the "solutions" directory (bearing the same filenames).

Try these commands:

    perl ./mine.perl -h
    perl ./mine.perl -r 2

The idea is to set your computer to "mine" a large number of puzzles while you go do something else.
When it's done, you can browse the puzzles and solutions and decide which ones you think are good
enough to keep.

What you do with them after that is up to you.  One easy idea would be to format them nicely,
print them out, and distribute them to your puzzle-loving friends.
