#!/usr/bin/perl -w

# find_common_sequences.perl
#
# Runs through the dictionary file specified in $dict_file and counts
# the number of times each four-letter sequence appears.  Reports any
# sequences that appear at least $n times.
#
# Suggested usage:
#   perl ./find_common_sequences.perl > common_sequences.txt

use v5.10;
use strict;
use warnings;
use Getopt::Long;



# Variables
my $cap = 2000;  # we only want to report this many sequences, even if there are more that meet the threshold
my $dict_file = "7dict.txt";
my $help = 0;
my $num_reported = 0;
my $report_counts = 0;  # whether to report the number of times each sequence appears
my %sequences;
my $threshold = 20;  # we're only interested in sequences that appear at least this many times

GetOptions(
  'cap|c=i'       => \$cap,
  'help|h'        => \$help,
  'numbers|n'     => \$report_counts,
  'threshold|t=i' => \$threshold,
);

if ($help) { print_help_and_exit(); }



### Iterate through the dictionary and collect all sequences
open( my $fh, "<:encoding(UTF-8)", $dict_file )
  or die "Can't open $dict_file for reading: $!";

while (<$fh>) {

  chomp;

  # Each seven-letter word contains four four-letter sequences.
  # This isn't the most elegant way to identify them, inasmuch as it would need to be edited if
  # we changed the lengths of the words in the dictionary, or of the sequences we're interested
  # in, but it has the virtues of being simple and short.

  /^(....)...$/;
  $sequences{$1}++;

  /^.(....)..$/;
  $sequences{$1}++;

  /^..(....).$/;
  $sequences{$1}++;

  /^...(....)$/;
  $sequences{$1}++;

}

close($fh);



### Iterate through the list of sequences and report the ones we're interested in
foreach my $seq (sort { $sequences{$b} <=> $sequences{$a} } keys %sequences) {

  # stop before reporting if this is the first sequence whose appearance rate is below the threshold
  if ( $sequences{$seq} < $threshold ) { last; }

  # make the report
  say $seq . ($report_counts ? ' ' . $sequences{$seq} : '');

  # stop after reporting if we have now reported as many sequences as the cap allows
  if (++$num_reported >= $cap) { last; }

}

### end of main program



sub print_help_and_exit {
print <<"END";
usage: $0 [-hn] [-c NUMBER] [-t NUMBER]
  -c\t--cap\t\tReport no more than this many sequences (default: 2,000)
  -h\t--help\t\tShow this help
  -n\t--number\tAlso report the number of times each sequence appears
  -t\t--threshold\tReport only sequences that appear at least this many times (default: 20)
END
exit 0;
}
