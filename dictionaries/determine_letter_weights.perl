#!/usr/bin/perl -w

# determine_letter_weights.perl
#
# Runs through the dictionary file specified in $dict_file
# and counts the number of times each letter appears.
# Produces output in a format that's easy to paste into a Ruby hash.

use v5.10;
use strict;
use warnings;

my $dict_file = "7dict.txt";

my %weights = (
  A => 0,  B => 0,  C => 0,  D => 0,  E => 0,  F => 0,
  G => 0,  H => 0,  I => 0,  J => 0,  K => 0,  L => 0,
  M => 0,  N => 0,  O => 0,  P => 0,  Q => 0,  R => 0,
  S => 0,  T => 0,  U => 0,  V => 0,  W => 0,  X => 0,
  Y => 0,  Z => 0,
);


open( my $fh, "<:encoding(UTF-8)", $dict_file )
  or die "Can't open $dict_file for reading: $!";

while (<$fh>) {
  chomp;
  foreach my $letter ( split //, $_ ) {
    $weights{$letter}++;
  }
}

close($fh);

foreach my $letter (sort keys %weights) {
  say "  '$letter': $weights{$letter},";
}
