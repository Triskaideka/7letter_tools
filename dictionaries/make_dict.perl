#!/usr/bin/perl -w

# make_dict.perl
#
# Runs through the dictionary files specified in @wordfiles,
# picks out all the 7-letter words, and writes a (unique,
# sorted) list of them to stdout.  Suggested usage:
#     perl ./make_dict.perl > 7dict.txt
#
# The ENABLE and SOWPODS files do not come with this
# repository.  You will have to download them from
# https://www.wordgamedictionary.com/dictionary/ , and/or
# edit the value of @wordfiles to use your own files.

use v5.10;

$/ = "\r\n";

my @wordfiles = ("enable.txt", "sowpods.txt");
my %words;

foreach my $wf (@wordfiles) {

  open( my $fh, "<:encoding(UTF-8)", $wf )
    or die "Can't open < $wf: $!";

  while (<$fh>) {
    chomp;

    # if this word contains 7 "word" characters, then we want it
    if ( $_ =~ /^[A-Za-z]{7}$/s ) {
      $words{ uc($_) } = 1;
    }
  }

  close($fh);

}

say join "\n", sort keys %words;
