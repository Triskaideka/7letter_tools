#!/usr/bin/ruby

=begin
compose.rb

Try to create a new puzzle of this type.
=end



### Libraries
require 'optparse'



### Input validation
options = {:embed => nil, :save => false, :save_only => false}

option_parser = OptionParser.new { |opts|

  opts.banner = "usage: #{$0} [-hsS] [-e <word>]"

  opts.on('-e', '--embed WORD', "Embed the provided word somewhere in the puzzle (not as a solution)") { |word|
    options[:embed] = word.upcase;
  }

  opts.on('-h', '--help', "Show this help") {
    puts opts
    exit
  }

  opts.on('-s', '--save', "Save the puzzle to a file") {
    options[:save] = true;
  }

  opts.on('-S', '--save-only', "Save puzzle to a file and don't print it on the screen") {
    options[:save_only] = true;
  }

}

option_parser.parse!
#puts options[:embed]



### Variables
prng = Random.new  # for generating (pseudo-)random numbers
suits = 4  # diamonds, clubs, hearts, spades
cards_per_suit = 13  # Ace through King is 13 cards.

# The puzzle grid is a two-dimensional array that holds the cards in the puzzle
grid = []

# Hash containing all the letters and how commonly they should appear.
# Got this distribution by analyzing the dictionary file using determine_letter_weights.perl.
letter_weights = {
  'A': 18464,
  'B': 5209,
  'C': 8057,
  'D': 9472,
  'E': 27218,
  'F': 3382,
  'G': 7490,
  'H': 5630,
  'I': 17857,
  'J': 644,
  'K': 3387,
  'L': 12345,
  'M': 6651,
  'N': 13906,
  'O': 13553,
  'P': 6698,
  'Q': 398,
  'R': 16584,
  'S': 21721,
  'T': 12874,
  'U': 8675,
  'V': 1978,
  'W': 2686,
  'X': 668,
  'Y': 3698,
  'Z': 1118
}

# put X copies of each letter into the pool, where X is its weight
pool = []
letter_weights.each { |letter, weight|
  weight.times { pool.push(letter.to_s) }
}
#puts pool.inspect

common_sequences = []
comseq_filename = "dictionaries/common_sequences.txt"
if ( File.exist?(comseq_filename) ) then
  comseq_file = File.open(comseq_filename, "r")
  common_sequences = comseq_file.readlines.map{ |line| line.chomp }
  comseq_file.close
else
  puts "The common sequences file expected to be found at #{comseq_filename} does not exist; proceeding without it."
end

# the number of books and runs to make from the common sequences
num_books_from_cs = 4
num_runs_from_cs = 6



### Initialize a grid of nil values
suits.times do |suit|

  grid.push( Array.new )

  cards_per_suit.times do |rank|
    grid[suit][rank] = pool.sample
  end

end
#puts grid.inspect



### Insert the common sequences.
# We don't really try to prevent the inserted sequences from overwriting parts
# (or all) of other sequences.  It's fine.  They're not divinely inspired, they're
# just a general attempt to make the puzzle a little friendlier.

# Insert some sequences as books.
book_ranks = (0...cards_per_suit).to_a.sample(num_books_from_cs)

book_ranks.each { |at_rank|

  # get one of the common sequences at random, make it an array,
  # and shuffle it (so it's not necessarily in order from top to bottom)
  seq = common_sequences.sample.split('').shuffle

  #puts "Inserting the sequence \"#{seq}\" as a book at rank #{at_rank}.";

  (0...suits).each { |s|
    grid[s][at_rank] = seq.pop
  }

}

# Insert some sequences as runs.
num_runs_from_cs.times do
  seq = common_sequences.sample
  at_suit = prng.rand(suits)
  at_rank = prng.rand( 0..(cards_per_suit - seq.length) )
  #puts "Inserting the sequence \"#{seq}\" as a run at #{at_suit}, #{at_rank}.";
  grid[at_suit][at_rank, seq.length] = seq.split('')
end
#puts grid.inspect



### Embed the user's word, if any (not as a possible solution, just as a "decoration").
# This may ruin some of the common sequences, but that's okay;
# it's important that the sequences don't ruin the embedding instead.
if ( options[:embed] ) then

  # first, check that the word will fit
  if ( options[:embed].length > cards_per_suit ) then
    raise "The word #{options[:embed]} is too long to fit in the puzzle"
  end

  # randomly select a viable location for it
  at_suit = prng.rand(suits)

  if (options[:embed].length === cards_per_suit) then
    at_rank = 0
  else
    at_rank = prng.rand(cards_per_suit - options[:embed].length)
  end

  # splice it in at the selected location
  grid[at_suit][at_rank, options[:embed].length] = options[:embed].split('')

end
#puts grid.inspect



### Output the grid, unless the user doesn't want it.
if ( ! options[:save_only] ) then
  grid.each { |suit|
    puts suit.join('')
  }
end



### Save the grid to a file, if requested.
if ( options[:save] || options[:save_only] ) then

  # Use the current time to generate a filename that is (very likely) unique and mostly meaningless.
  # Sending an argument to to_s() lets you specify the base in which to express the number.
  this_instant = Time.new
  outfilename = "puzzles/#{this_instant.to_i.to_s(36)}#{this_instant.nsec.to_s(36)}.txt"

  outfile = File.open(outfilename, "w")

  grid.each { |suit|
    outfile.puts( suit.join('') )
  }

  outfile.close

  puts "Saved puzzle in #{outfilename}"

end
