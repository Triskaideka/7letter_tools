#!/usr/bin/perl -w

# mine.perl
#
# "Mines" for usable puzzles by running compose.rb and then putting
# its output through solve.rb to see how many results are found.

use v5.10;
use strict;
use warnings;
use Getopt::Long;
use POSIX qw<floor>;



# Variables
my $compose_script = "./compose.rb";
my $solve_script = "./solve.rb";

my $output = '';  # for storing output of shell commands

my $puzzle_filename = '';  # the filename of the puzzle
my $solutions_filename = '';

my $start_time = time();
my $last_time = 0;  # for keeping track of how long each repetition takes

my $num_solutions = 0;
my $num_successes = 0;  # number of puzzles with enough solutions to meet the threshold
my $projected_duration = 0;  # how much longer we think it'll take to finish all the repetitions

my @solution_counts;  # so we can track the average number of solutions found
my @iteration_times;  # for estimating how much time is left



# Command-line options
my $embed = '';  # a word to embed in each puzzle (not as a possible solution, just as a "decoration")
my $embed_option = '';  # the string to use to pass along $embed to the compose script
my $help = 0;
my $reps = 1;  # number of times to run the compose/solve cycle
my $rep_limit = 9999;  # put a sensible cap on the program's running time
my $threshold = 15;  # minimum number of solutions that must be found in a puzzle to consider it worth keeping

Getopt::Long::Configure(qw<gnu_getopt>);

GetOptions(
  'embed|e=s'     => \$embed,
  'help|h'        => \$help,
  'reps|r=i'      => \$reps,
  'threshold|t=i' => \$threshold,
);



# Check for errors
if ($reps < 0) {
  print_help_and_exit();
}

if ($reps > $rep_limit) {
  say "This program could take 30 seconds or more per repetition.  ";
  say "If you really want to do more than $rep_limit repetitions, edit the \$rep_limit variable in this script.";
  exit 1;
}



# Only print the help, if it was requested.
if ($help) {
  print_help_and_exit();
}



# Begin output
say "Starting to compose and solve $reps puzzles.";


# Go through the compose/solve process as many times as asked.
while ($reps--) {

  $last_time = time();

  # execute the composer
  $embed_option = ($embed ? "-e $embed" : "");
  $output = qx<ruby $compose_script $embed_option -S>;

  # split the output string on whitespace and remember only the last token, which will be the filename
  $puzzle_filename = (split(/\s/, $output))[-1];

  # execute the solver
  $output = qx<ruby $solve_script -cpS $puzzle_filename>;

  print $output;

  unless ( $output =~ /^Saved ([0-9]+) solutions in (.+)$/ ) {
    say ("Solver produced unexpected output; moving on to next puzzle.");
    next;
  }

  # store the capture patterns from the above regex
  $num_solutions = $1;
  $solutions_filename = $2;

  push @solution_counts, $num_solutions;

  if ($num_solutions < $threshold) {

    say "That's not enough (threshold is $threshold).";
    unlink $puzzle_filename;
    unlink $solutions_filename;

  } else {

    $num_successes++;

  }

  # Record and report how long this repetition took
  push @iteration_times, (time() - $last_time);
  #if ( @iteration_times > 10 ) { shift @iteration_times; }  # only take the last 10 repetitions into account

  say "This iteration took " . $iteration_times[-1] . " seconds.";

  if ( $reps > 0 ) {
    $projected_duration = (average(@iteration_times) * $reps);
    say "Estimated time remaining: " . get_readable_time($projected_duration)
      . ".  If that's too long, use Ctrl+C to quit.";
  }

}

say "----------------------------------------------------------------------";
say "$num_successes puzzle(s) had at least $threshold solutions.";
say "There were an average of " . average(@solution_counts) . " solutions per puzzle.";
say "Average time to solve was " . average(@iteration_times) . " seconds per puzzle.";
say "Total time elapsed was " . get_readable_time(time() - $start_time) . ".";

### end of main program



# Copied from http://www.fiz-ix.com/2012/12/calculating-the-average-of-an-array-in-perl/
sub average {
  my @array = @_;  # save the array passed to this function
  my $sum;  # create a variable to hold the sum of the array's values
  foreach (@array) { $sum += $_; }  # add each element of the array to the sum
  return $sum/@array;  # divide sum by the number of elements in the array to find the mean
}



# Given a number of seconds, return that as a string enumerating the number of hours, minutes, and seconds.
sub get_readable_time {
  my $total_seconds = shift;

  my $hours = floor($total_seconds / 60 / 60);
  my $minutes = floor($total_seconds / 60 ) % 60;
  my $seconds = $total_seconds % 60;

  return "${hours}h ${minutes}m ${seconds}s";
}



sub print_help_and_exit {
print <<"END";
usage: $0 [-h] [-e word] [-r reps] [-t threshold]
  -e\t--embed\t\tEmbed this word into every puzzle
  -h\t--help\t\tShow this help
  -r\t--reps\t\tHow many repetitions to do (default: 1)
  -t\t--threshold\tHow many solutions makes a puzzle worth keeping? (default: 15)
END
exit 0;
}
